//
//  Const.h
//  Jordan Amen
//
//  Created by Developer on 1/12/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Const : NSObject

#define FROM_MAIL                   111
#define FROM_PHONE                  222
#define FROM_WHATSAPP               333
#define FROM_FREQ                   444

@end
