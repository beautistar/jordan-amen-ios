//
//  MailViewController.m
//  Jordan Amen
//
//  Created by Developer on 1/12/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MailViewController.h"

#import "Const.h"

@interface MailViewController () {
    
    __weak IBOutlet UIImageView *imvBg;
    
}

@end

@implementation MailViewController
@synthesize _from;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
    
    switch (_from) {
        case FROM_MAIL:
            [imvBg setImage:[UIImage imageNamed:@"bg_sms"]];
            break;
            
        case FROM_WHATSAPP:
            [imvBg setImage:[UIImage imageNamed:@"bg_whatsapp"]];
            break;
            
        case FROM_PHONE:
            [imvBg setImage:[UIImage imageNamed:@"bg_number"]];
            break;
            
        case FROM_FREQ:
            [imvBg setImage:[UIImage imageNamed:@"bg_freq"]];
            break;        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
