//
//  MainViewController.m
//  Jordan Amen
//
//  Created by Developer on 1/12/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MainViewController.h"
#import "MailViewController.h"
#import "Const.h"
#import "FLAnimatedImage.h"

@interface MainViewController () {
    
    float _iconWidth;
    __weak IBOutlet NSLayoutConstraint *barViewHeightConstrain;
    
    MPMoviePlayerController *player;

    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIView *visualizerView;
    __weak IBOutlet FLAnimatedImageView *imvVisualizer;
    
    AVAudioPlayer *aPlayer;
    
    AVAudioPlayer *audioPlayer;
    __weak IBOutlet NSLayoutConstraint *vHeight;
    
    NSURL *url;
    
    __weak IBOutlet UIView *contentView;
    
    MPMoviePlayerController * mplayer;

    MPMoviePlayerController *mp;
    NSTimer *visualizerTimer;
    BOOL isAudioPlaying;

}

@end

@implementation MainViewController
@synthesize isVideoPlaying;

- (void)viewDidLoad {

//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [[AVAudioSession sharedInstance] setActive: YES error: nil];
//    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [super viewDidLoad];
    
    isAudioPlaying = NO;
    isVideoPlaying = NO;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    _iconWidth = ([UIScreen mainScreen].bounds.size.width - 50.0) / 7.0;
    
    barViewHeightConstrain.constant = _iconWidth + (_iconWidth / 8.0) + 10;
    
    [self initAudioView];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
//    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
//    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        //vHeight.constant = 60.0;
    } else {
        //vHeight.constant = 80;
    }
    
    _iconWidth = ([UIScreen mainScreen].bounds.size.width - 50.0) / 7.0;
    
    barViewHeightConstrain.constant = _iconWidth + (_iconWidth / 8.0) + 10;

    [mplayer stop];
}

- (void) initAudioView {
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"200" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    imvVisualizer.animatedImage = animatedImage1;

}

- (IBAction)tvAction:(id)sender {
    
    [self playVideo];
}
- (IBAction)FMPlayerAction:(id)sender {

    [self playAudio];
    isAudioPlaying = !isAudioPlaying;
}

- (void) playVideo {
    
    isVideoPlaying = YES;
    [contentView setHidden:NO];
    [btnClose setHidden:NO];
    
    NSURL *movieURL = [NSURL URLWithString:@"http://streaming.i-sat.tv:1935/live/amn/playlist.m3u8"];
    MPMoviePlayerController *vmp = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    [self becomeFirstResponder];
    
    if (vmp)
    {
        vmp.view.frame = contentView.bounds;
        [contentView addSubview:vmp.view];
        [vmp setMovieSourceType:MPMovieSourceTypeStreaming];
        [vmp setAllowsAirPlay:YES];
        
        // save the movie player object
        [vmp setFullscreen:YES];
        
        // Play the movie!
        [vmp play];
        mplayer = vmp;
    }
}

- (void) stopAudio {
    
    [visualizerView setHidden:YES];
    [mp pause];
}

- (void) playAudio {

    [btnClose setHidden:NO];
   
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    /////////////
    NSURL *movieURL = [NSURL URLWithString:@"http://streaming.i-sat.tv:1935/live/amn/playlist.m3u8"];
    mp = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    [self becomeFirstResponder];
    
    if (mp)
    {
        mp.view.frame = contentView.bounds;
//        [self.view addSubview:mp.view];
//        [contentView addSubview:mp.view];
        [mp setMovieSourceType:MPMovieSourceTypeStreaming];
        [mp setAllowsAirPlay:YES];
        
        // save the movie player object
        [mp setFullscreen:NO];
        
        // Play the movie!
        [mp play];
        mplayer = mp;        
    }
    
    [contentView setHidden:YES];
    
    // for visualizer video play
    [visualizerView setHidden:NO];
}

-(void) remoteControlReceivedWithEvent:(UIEvent *)event{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoteControlEventReceived" object:event];
    
//    if (event.type == UIEventTypeRemoteControl){
//        switch (event.subtype) {
//            case UIEventSubtypeRemoteControlTogglePlayPause:
//                // Toggle play pause
//                
//                [player play];
//                break;
//            default:
//                break;
//        }
//    }
}

-(void)remoteControlEventNotification:(NSNotification *)note{
    UIEvent *event = note.object;
    if (event.type == UIEventTypeRemoteControl){
        switch (event.subtype) {
            case UIEventSubtypeRemoteControlTogglePlayPause:
                if (mplayer.playbackState == MPMoviePlaybackStatePlaying){
                    [mplayer pause];
                } else {
                    [mplayer play];
                }
                break;
                // You get the idea.
            default:
                break;
        }
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    return YES;

}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [player.view removeFromSuperview];
}

- (IBAction)facebokAction:(id)sender {
    
    NSURL *_url = [NSURL URLWithString:@"https:/www.facebook.com/radio.amenfm89.5"];
    [[UIApplication sharedApplication] openURL:_url];
    
}

- (IBAction)webAction:(id)sender {
    
    NSURL *_url = [NSURL URLWithString:@"http://www.amenfm.jo"];
    [[UIApplication sharedApplication] openURL:_url];
}

- (IBAction)youtubeAction:(id)sender {
    
    NSURL *_url = [NSURL URLWithString:@"http://www.youtube.com/user/amenfm1"];
    [[UIApplication sharedApplication] openURL:_url];
}

- (IBAction)mailAction:(id)sender {
    
    MailViewController * targetVC = (MailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MailViewController"];
    targetVC._from = FROM_MAIL;
    [self.navigationController pushViewController:targetVC animated:NO];
}

- (IBAction)phoneAction:(id)sender {
    
    MailViewController * targetVC = (MailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MailViewController"];
    targetVC._from = FROM_PHONE;
    [self.navigationController pushViewController:targetVC animated:NO];
}

- (IBAction)whatsappAction:(id)sender {
    
    MailViewController * targetVC = (MailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MailViewController"];
    targetVC._from = FROM_WHATSAPP;
    [self.navigationController pushViewController:targetVC animated:NO];
}


- (IBAction)freqAction:(id)sender {
    
    MailViewController * targetVC = (MailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MailViewController"];
    targetVC._from = FROM_FREQ;
    [self.navigationController pushViewController:targetVC animated:NO];
}

- (IBAction)closeAction:(id)sender {
    
    [mplayer stop];
    [contentView setHidden:YES];
    [visualizerView setHidden:YES];
    [btnClose setHidden:YES];
    isVideoPlaying = NO;
    isAudioPlaying = NO;
}

@end
