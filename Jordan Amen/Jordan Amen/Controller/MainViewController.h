//
//  MainViewController.h
//  Jordan Amen
//
//  Created by Developer on 1/12/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MainViewController : UIViewController
@property (nonatomic) BOOL isVideoPlaying;

@end
